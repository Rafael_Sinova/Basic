﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.2">
  <GVL Name="variables" Id="{6b224b76-11a0-478b-a629-4e24bf53ce5d}">
    <Declaration><![CDATA[{attribute 'qualified_only'}
VAR_GLOBAL

// Variables
	OperationMode			: INT;	// Actual AGV operation mode
	OperationStatus			: INT;  // Actual AGV operational status
	IgnitionState			: UINT; // Ignition 0=Estop, 1=Estop clear, 2=Estop reseted, 3=started  
	IhmPlcIgnition			: BOOL;	// Ignition command sended by Ihm
	DriveInterlockState		: UINT;	// Drive and steer interlock, 0=not enabled, 1=interlock released, 2=main contactor colsed
	PumpInterlockState		: UINT;	// Pump interlock, 0=not enabled, 1=interlock released, 2=main contactor colsed
	actualKeySwitchVoltage 	: INT;	// actual driver key switch voltage 		
	BatteryLevel			: INT;	// Indicates the actual battey level
	AbovePosition			: BOOL; // AGV positioned over a tag
	ActualSpeed				: INT; 	// Actual movement speed
	InPosition				: BOOL;	// AGV is in position
	LiftInPosition			: BOOL;	// Lifting device is in position
	ActualLiftHight			: INT;	// Actual position of the lifting axis
	loadDetectionHight		: INT;  // Hight of lift when load presence was triggered
	Loaded					: INT;	// AGV loaded bit0 if only one load, bit 1-7 if several loads
	ScannerWarningField1	: BOOL;  // Safety scanner warning field 1
	ScannerWarningField2	: BOOL;  // Safety scanner warning field 2  
	BypassMode				: BOOL;	// ByPass Button pressed  
	LostPaletError 			: BOOL;	// Palet lost fault from Flexisoft 
	driveInNavigationMode	:BOOL;  // Drive in navigation mode 
//Alarm
	AlarmCode				: INT; 	// code of the screened alarm  
	AlarmMessage			: STRING; // text of the displayed alarm	
	AlarmLevel				: INT;	// level of the displayed alarm  

//Communication variables
	HeartBeatIhm			: BOOL; // Heart beat from IhmAGV
	HeartBeatIhmOk			: BOOL; // Heart beat from IhmAGV is OK
	HeartBeatTw				: BOOL; // Heart beat from TW
	HeartBeatTwOk			: BOOL; // Heart beat from TW is OK
	
//IhmAGV variables
	IhmVersion				: STRING; // Version number of the IhmAGV 
	IhmTagNumberWriteCode	: STRING; // number to be written to the TFID tag

//Quality Management
	Qalert					: BOOL; // Quality alert selected
	Qstop					: BOOL; // Quality stop selected

//Maintenance
	NextMaintenanceHours	: DINT;
	NextMaintenanceDate		: STRING[10];

//Mission Variables
	NewJobBit				: BOOL; // Triggers the start of a mission
	TargetPosition			: DINT; // Target position send by TW via IhmAGV 
	TargetAction			: INT;	// Target action send by TW via IhmAGV
	TargetActionVar			: INT;	// Auxiliar variable for target ation send by TW via IhmAGV
	AimPosition				: DINT;	// Aim position set for mission based on received target 
	AimAction				: INT;	// Aim action set for mission based on received target
	AimActionVar			: INT;	// Aim action variable set for mission based on received target
	PreviousPosition		: DINT; // Previous readed position
	NextPosition			: DINT; // Next expected position
	AbortJob				: BOOL; // Job abort command send by TW via IhmAGV
	PauseJob				: BOOL;	// Pause command send by TW via IhmAGV
	PauseJobAGV				: BOOL; // Pause command triggered by PLC
	PauseJobSafeZone		: BOOL;	// Pause command in a safty zone when no communication 
	LockRelease				: BOOL;	// Relaese of a position lock send by TW via IhmAGV
	WaitingLockRelease		: BOOL; // Waiting status send by PLC to IhmAGV 
	StartMove				: BOOL; // Triiger the movement start in auto mode
	ExecutingMove			: BOOL; // AGV moving ack
	AutoStopStart			: BOOL; // Trigger the AGV stopping in auto mode 
	AutoStopOK				: BOOL; // AGV stopped ack
	ActionStart				: BOOL; // Triggers the action after a aim position is reached
	ActionCompleted			: BOOL; // Confirmation that action is completed
	OperatorRelease			: BOOL; // Relaese triggered by PLC
	WaitingRelease			: BOOL; // Waiting status send by PLC to IhmAGV
	IhmPlcRelease			: BOOL; // Release command send by IhmAGV
	PcPlcRelease			: BOOL;	// Release command send by TW 
	ButtonRelease			: BOOL; // Release request by PLC 
	TurtleSpeedCmdBit		: BOOL; // Turtle speed activated by IhmAGV	
	JobElapsedTime			: INT;  // Time elapsed since last mission start
	moveInBox				: BOOL; 
	byPassScannerSide		:BOOL;  // 
	
//Route Variables
	RouteTab				: RouteRuleStatic;
	NextMoveTab				: RouteRuleStatic;
	MoveTab					: RouteRuleStatic;
	MoveDynamic				: RouteRuleDynamic;
	AimTabCmdBit			: BOOL;
	
//AGV Navigation
	DefaultTrack			: INT;  // Track to be followed  bit 1=Lane 1, etc
	SelectedTrack			: INT;  // Track actually selected bit 1=Lane 1, etc
	TrackDetectedFront		: BYTE; // Lane detected front antena bit 1=Lane 1, etc
	TrackDetectedRear		: BYTE; // Lane detected rear antena bit 1=Lane 1, etc
	LastReadedOffsetFront	: REAL;	// Last detected offset value before loosing track - front
	LastReadedOffsetRear	: REAL;	// Last detected offset value before loosing track - rear
	track1LevelFront		: INT;  // intensity in% of the track signal - front antenna 
	track2LevelFront		: INT;
	track3LevelFront		: INT;	
	track1LevelRear			: INT;  // intensity in% of the track signal - rear antenna
	track2LevelRear			: INT;
	track3LevelRear			: INT;	
	
//LGV Navigation 
	OnTrack					: BOOL; // AGV is on track (from IhmAGV to PLC)
	deccelDist				: DINT; // estimated desaceleration distance to control deccel speed  
	ClearRoute				: BOOL; // allows IhmAGV to redefine the actual position after a unload position shift 
	AbsolutPositionX		: REAL; // absolut x position of AGV in global coordinate system 
	AbsolutPositionY		: REAL; // absolut y position of AGV in global coordinate system 
	AbsolutPositionAngle	: REAL; // absolut angular position of AGV in global coordinate system 
	WaitingLockAreas		: STRING; //used only by IhmAGV
	WaitingLockAreasRelease : STRING; //used only by IhmAGV
	
//Motion
	speedWhenReadingTag		:INT;  // speed when passing over a tag
	travelDistance			:DINT; // travelled disntence since last tag was readed
	travelDistanceAtLock	:DINT; // travelled disntence when a lock condition is rised 
	driveBackwards			:BOOL; // AGV driving backwards
	ActualSteeringWheelAngle:REAL; // Actual steering wheel angel 
	BoxAproaching			:BOOL; // Approaching a box - triggers a lift hight adjustement  (MoveDistanceToTarget < DistanceToBox + DistanceToApproachBox)  
	BoxDrivingIn 			:BOOL; // Drining in to a box zone (MoveDistanceToTarget < DistanceToBox)
	BoxDrivingOut			:BOOL; // Driving out of a box (Travel Distance < DistanceToBox)
	RowApproaching			:BOOL; // Approaching a row - triggers a lift hight adjustement  (MoveDistanceToTarget < rowActionStartDistance + RowUnlodingApproachDistance)
	RowActionZone			:BOOL; // Driving in a row - search load (rowActionStartDistance <> 0 AND MoveDistanceToTarget < rowActionStartDistance)
	outputSpeed				:REAL; // Drive control output speed

//ForkLift
	LiftSafelyUnlocked		: BOOL; //Safety unlocks the manual lift - Pulse 3 times the manual lift button within 2 seconds 
	DownSafelyUnlocked		: BOOL; //Safety unlocks the manual down - Pulse 3 times the manual down button within 2 seconds

//Automatic Battery Exchange
	AutoBatteryExchangeRelease:BOOL; // AGV is ready for automatic battery exchange
	AutoBatteryExchangeAck	:BOOL; // Battery exchange acknowledgment	
	
//Sensors
	LoadApproachSensor		:BOOL; // Load detection to unloading in a row
	loadDistance			:INT; //distance in mm to load 

// Remote Control via APP
	RCMoveAngle				: REAL; // Remote control - steering angle
	RCMoveSpeed				: REAL; // Remote control - drive speed 
	RCMoveAuxDirection		: REAL; // Remote control - auxiliar direction rear steering
	RCMoveMovingKick		: BOOL; // Remote control - toggle bit 
	
//Conveyors
	ConveyorsPlcTw			: ConveyorsPlcTw; // ARRAY[1..10] OF ConveyorPlcTw
	ConveyorsTwPlc			: ConveyorsTwPlc; // ARRAY[1..10] OF ConveyorTwPlc
	
//LECTOR 62

	PalletNrRequested		: STRING;
LaserConfigurationMode 			:BOOL;
	
	
END_VAR

VAR_GLOBAL PERSISTENT
	ActualPosition			: DINT;	// Actual AGV position - last tag readed
	pcRouteTableVersion		: STRING; // route table version
	travelDistanceCM		: DINT;   // tarveled distance since last tag reading - in cm
	travelDistanceMM		: DINT;   // tarveled distance since last tag reading - in mm
	ProductCode				: STRING; // Code number of the loaded product	
	batteryChargingStations	: STRING; //list of charging positions 
	Hourmeter				: UDINT;  //operation hours	
	Secondmeter				: UDINT;  // operation seconds	
END_VAR]]></Declaration>
  </GVL>
</TcPlcObject>