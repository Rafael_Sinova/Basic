﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.3">
  <POU Name="FB_MissionMgt" Id="{4b8c37c8-a4d7-4f0b-a4c3-b859113c5d44}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_MissionMgt
VAR_INPUT
END_VAR
VAR_OUTPUT
END_VAR
VAR
	step					: INT;
	blinkingRTrig			: R_TRIG;
	blinkingCount			: INT;
	buttonJobRestart		: TON;
	nn						: INT;
	missionTosamePos		:BOOL;


END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[//////////////////////////////////////////////////////////////////////////
//                           Mission Manager                            //
//////////////////////////////////////////////////////////////////////////
//
//Mission Steps:
// 1:  Register Mission
// 2:  Request Movement data do IhmAGV
// 3:  Register response from IhmAGV
// 4:  Start Movement
// 5:  Request next Movement data do IhmAGV
// 6:  Register response from IhmAGV
// 7:  Move till next tag is reached - AGV
// 8:  Move till next tag is reached - LGV 
// 9:  Stop
// 10: Execut	e action at aim position
// 11: End and clear mission;


//Start Mission
IF variables.OperationMode = 3 AND  variables.OperationStatus = 1
    AND (variables.TargetPosition <> variables.AimPosition OR variables.TargetAction <> variables.AimAction)
    AND variables.NewJobBit THEN
    step := 1; // Start Mission
END_IF;

(*buttonJobRestart(IN:=variables.OperationMode=3 AND variables.TargetPosition<>0 AND IOs.diStartButton, PT:=T#5S , Q=> , ET=> );
IF buttonJobRestart.Q THEN
	variables.NewJobBit:=TRUE;
END_IF*)

//Cancel Mission
IF variables.OperationMode <> 3 THEN
    step := 11; // Cancel Mission
END_IF;

//Movement announcement blinkings 
blinkingRTrig(CLK := IOs.doLampGreen);
IF blinkingCount < parameters.BlinkingBeforeDrive + 2 AND blinkingRTrig.Q THEN
	blinkingCount := blinkingCount + 1;
END_IF;

//Mission Steps
CASE step OF
    1:  //// Register Mission
        IF variables.TargetPosition <> 0 AND variables.NewJobBit THEN
            variables.AimPosition := variables.TargetPosition;
            variables.AimAction := variables.TargetAction;
            variables.AbortJob := FALSE;
            IF variables.ActualPosition = variables.AimPosition AND parameters.AgvType='AGV' THEN
                step := 7;
            ELSIF variables.ActualPosition = variables.AimPosition AND parameters.AgvType='LGV' THEN
                //step := 8;
				step:=9;
				missionToSamePos:=TRUE;
            ELSE
                step := 2;
            END_IF;
        ELSE
            step := 0;
        END_IF;
        
    2:  //// Request Movement data do IhmAGV
		variables.RouteTab.actualPosition:= variables.AimPosition;
        variables.RouteTab.actualPosition:= variables.ActualPosition;
		variables.RouteTab.tabError:=FALSE;
		IF parameters.AgvType='LGV' THEN
			variables.MoveDynamic.MoveDistanceToTarget:=300;
			variables.MoveDynamic.MoveDistanceToReverse:=300;
		END_IF
        variables.AimTabCmdBit := TRUE;
        step := 3;
        
    3:  //// Register response from IhmAGV
        IF NOT variables.AimTabCmdBit AND NOT variables.RouteTab.tabError THEN
			variables.MoveTab := variables.RouteTab;
			IF variables.RouteTab.moveType >0 THEN
				alarms.faultNoRoutTabMoveType:=FALSE;
				variables.RouteTab.tabError:=FALSE;
				step := 4;
			ELSE
				alarms.faultNoRoutTabMoveType:=TRUE;
            END_IF
		ELSIF NOT variables.AimTabCmdBit AND variables.RouteTab.tabError THEN
			 	alarms.faultRouteTabError:= TRUE;
		END_IF	
	                   
    4: //// Start Movement
        IF NOT variables.ExecutingMove THEN
			IF blinkingCount >= parameters.BlinkingBeforeDrive THEN
                variables.StartMove := TRUE;    // Release Movement
            END_IF;
        END_IF;
        
        IF variables.ExecutingMove THEN
            IF	parameters.AgvType='LGV' THEN
				step := 8; //Execute last movement segment
			ELSIF variables.RouteTab.nextPosition = variables.AimPosition THEN
                step := 7; //Execute last movement segment
            ELSE
                step := 5; //Execute next movement segment
            END_IF    
            variables.PreviousPosition := variables.ActualPosition;
        END_IF;
        
		IF parameters.AgvType='LGV' THEN
			variables.MoveTab:= variables.RouteTab;
		END_IF
        
    5:  //// Request next Movement data do IhmAGV
		variables.RouteTab.actualPosition:= variables.AimPosition;
        variables.RouteTab.actualPosition:= variables.NextPosition;
		variables.RouteTab.tabError:=FALSE;
        variables.AimTabCmdBit := TRUE;        
        step := 6;
        
    6:  //// Register response from IhmAGV
        IF NOT variables.AimTabCmdBit AND NOT variables.RouteTab.tabError THEN
			variables.NextMoveTab := variables.RouteTab;           
         	step := 7;
        END_IF;
        
    7:  //// Move till next tag is reached - AGV
		IF variables.AbovePosition THEN //Load next movement
			IF variables.ActualPosition = variables.NextPosition
				AND variables.ActualPosition <> variables.AimPosition
				AND NOT variables.RouteTab.tabError THEN
				variables.MoveTab:=variables.NextMoveTab;  //Tag corresponds to expected Tag and isnt aim position
				variables.ExecutingMove := FALSE;
				step := 4;
			ELSIF variables.ActualPosition = variables.AimPosition
			(*OR (variables.ActualPosition <> variables.PreviousPosition AND variables.ActualPosition <> variables.NextPosition)*)
			OR (variables.ActualPosition <> variables.PreviousPosition AND variables.RouteTab.tabError) THEN
				variables.ExecutingMove := FALSE;   //Start stopping
				step := 9;
			END_IF
		END_IF
		
		IF (variables.ActualPosition = variables.NextPosition AND NOT variables.AbovePosition) OR variables.AbortJob THEN
			step := 9; //Start stopping
		END_IF	
			
		//Lock
        IF variables.RouteTab.moveLock > 0 THEN
            IF NOT variables.LockRelease THEN   //Request Lock
                variables.WaitingLockRelease := TRUE;
			END_IF;
            IF NOT variables.LockRelease THEN  //Relase
                variables.WaitingLockRelease := FALSE;
                variables.LockRelease := FALSE;
				IF variables.ActualSpeed=0 THEN
					variables.ExecutingMove:=FALSE;
					blinkingCount:=0;
					step := 4;
				END_IF	
            END_IF;
        END_IF
			
    8:	//// Move till next tag is reached - LGV 
		IF variables.MoveTab.moveType <> variables.RouteTab.moveType THEN
			variables.ExecutingMove:=FALSE; //Change movement type
			variables.StartMove:=TRUE;
		END_IF
	
		variables.MoveTab:= variables.RouteTab;
		IF (variables.MoveDynamic.MoveDistanceToTarget<parameters.ApproachDistance AND variables.ActualPosition=variables.AimPosition )
		OR (variables.MoveDynamic.MoveDistanceToTarget<parameters.LoadSeekingDistance AND variables.AimAction=1 AND parameters.LoadSeekingEnable)
		OR (variables.RowActionZone AND variables.AimAction=1 AND variables.Loaded>0)
		OR (variables.RowActionZone AND variables.AimAction=2 AND variables.LoadApproachSensor)
		OR variables.AbortJob THEN
			variables.ExecutingMove:=FALSE;
			step := 9; //Start stopping
		END_IF
	


		IF variables.RouteTab.moveLock > 0 THEN
            IF NOT variables.LockRelease THEN   //Request Lock
                variables.WaitingLockRelease := TRUE;
            END_IF;
            IF NOT variables.LockRelease THEN  //Relase
                variables.WaitingLockRelease := FALSE;
                variables.LockRelease := FALSE;
			END_IF;
		END_IF;
       
		variables.MoveTab:= variables.RouteTab;

	9:  //// Stop
	IF NOT missionTosamePos THEN
        variables.AutoStopStart := TRUE;	
END_IF;		
		IF parameters.AgvType='LGV' THEN
			variables.MoveTab:= variables.RouteTab;
		END_IF
		
        IF variables.AutoStopOK  OR missionTosamePos THEN
			IF parameters.AgvType='AGV' THEN
				variables.NextPosition := 0;	
			END_IF
			variables.AutoStopStart := FALSE;
			variables.AutoStopOK := FALSE;
			

			            
            IF variables.ActualPosition = variables.AimPosition 
			OR (variables.RowActionZone AND variables.AimAction=1 AND variables.Loaded>0)
			OR (variables.RowActionZone AND variables.AimAction=2 AND variables.LoadApproachSensor) THEN
                step := 10;    // Expected tag and aim position
            ELSIF variables.AbortJob THEN
                step := 11;    //Job aborted
			ELSIF	variables.ActualPosition <> variables.NextPosition AND variables.ActualPosition <> variables.AimPosition AND parameters.EnableUnexpectedTagFault THEN
				alarms.faultUnexpectedTag:=TRUE;				
            ELSIF variables.ActualPosition <> variables.NextPosition THEN
                step := 2;    //Recalculate
            END_IF;
        END_IF;
      
    10: //// Execute action at aim position
        IF NOT MAIN.fbForkliftActionMgt.unloadPositionShift AND variables.AimAction<>8 THEN
			drive.setSpeed:=0;
			drive.steerModeFront:=0;
			drive.steerModeRear:=0;	
		END_IF
		variables.MoveTab.moveType := 0;
        variables.ActionStart := TRUE;
        
        IF variables.AimAction = 0 THEN
            variables.ActionCompleted := TRUE;   // No action
        ELSIF variables.AimAction >= 1 AND variables.AimAction <= 4 THEN
            ;  //Execute action
        ELSIF variables.AimAction = 5 THEN   
            variables.WaitingRelease := TRUE;
            IF variables.OperatorRelease OR variables.IhmPlcRelease OR variables.PcPlcRelease THEN
                variables.ActionCompleted := TRUE;
            END_IF;
        END_IF;
        
        IF variables.ActionCompleted OR variables.AbortJob THEN
            step := 11;
        END_IF;
        
    11: //// End and clear mission
		alarms.faultNoRoutTabMoveType:=FALSE;
		alarms.faultUnexpectedTag:=FALSE;
		alarms.faultRouteTabError:=FALSE;
		variables.ExecutingMove := FALSE;
		variables.AimPosition := 0;
		variables.AimAction := 0;
		variables.NewJobBit := 0;
		variables.AimTabCmdBit := FALSE;
		variables.AbortJob := 0;
		variables.NextPosition := 0;
		variables.MoveTab.moveType := 0;
		variables.AutoStopStart := FALSE;
		variables.AutoStopOK := FALSE;
		variables.WaitingLockRelease := FALSE;
		variables.WaitingRelease := FALSE;
		variables.ClearRoute := TRUE;
		variables.ActionStart := FALSE;
		variables.ActionCompleted := FALSE;
		variables.PcPlcRelease := FALSE;
		variables.IhmPlcRelease := FALSE;
		blinkingCount := 0;
		step := 0;
		missionTosamePos:=FALSE;
END_CASE;]]></ST>
    </Implementation>
    <LineIds Name="FB_MissionMgt">
      <LineId Id="484" Count="15" />
      <LineId Id="747" Count="0" />
      <LineId Id="501" Count="34" />
      <LineId Id="881" Count="0" />
      <LineId Id="884" Count="0" />
      <LineId Id="536" Count="103" />
      <LineId Id="811" Count="2" />
      <LineId Id="809" Count="1" />
      <LineId Id="778" Count="0" />
      <LineId Id="640" Count="0" />
      <LineId Id="841" Count="0" />
      <LineId Id="642" Count="1" />
      <LineId Id="854" Count="0" />
      <LineId Id="645" Count="3" />
      <LineId Id="652" Count="14" />
      <LineId Id="886" Count="0" />
      <LineId Id="780" Count="0" />
      <LineId Id="888" Count="0" />
      <LineId Id="782" Count="0" />
      <LineId Id="781" Count="0" />
      <LineId Id="667" Count="0" />
      <LineId Id="779" Count="0" />
      <LineId Id="668" Count="5" />
      <LineId Id="773" Count="2" />
      <LineId Id="675" Count="0" />
      <LineId Id="827" Count="1" />
      <LineId Id="676" Count="8" />
      <LineId Id="690" Count="32" />
      <LineId Id="760" Count="0" />
      <LineId Id="723" Count="6" />
      <LineId Id="867" Count="0" />
      <LineId Id="730" Count="5" />
      <LineId Id="883" Count="0" />
      <LineId Id="9" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>