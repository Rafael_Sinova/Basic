﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.2">
  <POU Name="FB_AlarmMgt" Id="{b48a16ef-0ca5-4a7c-b22a-d5b86c9c6d93}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_AlarmMgt
VAR_INPUT
END_VAR
VAR_OUTPUT
END_VAR
VAR
	pulse				: TON;
	i					: INT;
	n					: INT;
	test: STRING;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[//////////////////////////////////////////////////////////////////////////
//                             Alarm Manager                            //
//////////////////////////////////////////////////////////////////////////
//
//Alarm groups
// 001 - 019 => System Alarms - Ethercat / CanOPEN / Communication / Battery
// 020 - 039 => Safety Alarms - 
// 040 - 059 => Motion Alarms - Power Controllers / Drive, Steer, Lift - Timeouts
// 060 - 079 => Sensor Alarms - Inductive / MLS / Lector
// 080 - 099 => Mission Management Alarms
// 100 - 119 => Movement Faults Alarms
// 120 - 139 => Action Alarms

//Alarm Level
// 1= warning messages
// 2= process fail => movement and sequence stop
// 3= system or eStop fail => resets ignition



//System Fault
alarms.group[1].state:=alarms.faultEthercat;
alarms.group[1].code:=1;
alarms.group[1].level:=3;
alarms.group[1].priority:=3;
alarms.group[1].message:='Falha EtherCAT';

alarms.group[2].state:=alarms.faultCanOpenMaster;
alarms.group[2].code:=2;
alarms.group[2].level:=3;
alarms.group[2].priority:=3;
alarms.group[2].message:='Falha CANopen - Mestre';

//Communication
alarms.group[3].state:=alarms.faultHeartBeatIhm;
alarms.group[3].code:=3;
IF variables.OperationMode=3 THEN
	alarms.group[3].level:=2;
	alarms.group[3].priority:=2;
ELSE
	alarms.group[3].level:=1;
	alarms.group[3].priority:=1;	
END_IF
alarms.group[3].message:='Sem Comunicação com IhmAGV';

alarms.group[4].state:=variables.IgnitionState=1;
alarms.group[4].code:=4;
alarms.group[4].level:=1;
alarms.group[4].priority:=1;
alarms.group[4].message:='Sem Rearme';

alarms.group[5].state:=variables.IgnitionState=2;
alarms.group[5].code:=5;
alarms.group[5].level:=1;
alarms.group[5].priority:=1;
alarms.group[5].message:='Sem Start';

alarms.group[6].state:=alarms.faultHeartBeatTw;
alarms.group[6].code:=6;
IF parameters.EnableStopIfNoTwComm THEN
	alarms.Group[6].level:=2;
	alarms.group[6].priority:=2;
ELSE
	alarms.Group[6].level:=1;	
	alarms.group[6].priority:=1;	
END_IF
alarms.group[6].message:='Sem Comunicação com Gerenciador';


//Battery Management
alarms.group[7].state:=alarms.warningLowBattery;
alarms.group[7].code:=7;
alarms.group[7].level:=1;
alarms.group[7].priority:=1;
alarms.group[7].message:='Alarme - Carga Bateria Baixa';

alarms.group[8].state:=alarms.faultBatteryEmpty;
alarms.group[8].code:=82;
alarms.group[8].level:=2;
alarms.group[8].priority:=2;
alarms.group[8].message:='Bateria Descaregada';


//Safety Management
alarms.group[20].state:=alarms.faultDoorsOpen<>0;
alarms.group[20].code:=20;
alarms.group[20].level:=3;
alarms.group[20].priority:=3;
alarms.group[20].message:='Tampa Aberta - ';
FOR n:=0 TO 3 DO
	IF SHR(SHL(UINT_TO_BYTE(alarms.faultDoorsOpen),7-n),7)=1 THEN 
		alarms.group[20].message:= concat(alarms.group[20].message, parameters.DoorsAlarmLabel[n]); // Array Test
		alarms.group[20].message:= concat(alarms.group[20].message,' / ');
	END_IF
	IF n=3 THEN
		alarms.group[20].message := Delete(alarms.group[20].message,3,len(alarms.group[20].message)-2);
	END_IF
END_FOR

alarms.group[21].state:=alarms.faultBatteryLockOpen<>0;
alarms.group[21].code:=21;
alarms.group[21].level:=3;
alarms.group[21].priority:=3;
alarms.group[21].message:='Trava da Bateria Aberta - ';
FOR n:=0 TO 3 DO
	IF SHR(SHL(UINT_TO_BYTE(alarms.faultBatteryLockOpen),7-n),7)=1 THEN 
		alarms.group[21].message:= concat(alarms.group[21].message, parameters.BatteryLockAlarmLabel[n]); // Array Test
		alarms.group[21].message:= concat(alarms.group[21].message,' / ');
	END_IF
	IF n=3 THEN
		alarms.group[21].message := Delete(alarms.group[21].message,3,len(alarms.group[21].message)-2);
	END_IF
END_FOR

alarms.group[22].state:=alarms.eStopButton<>0;
alarms.group[22].code:=22;
alarms.group[22].level:=3;
alarms.group[22].priority:=3;
alarms.group[22].message:='Botão de Emergencia - ';
FOR n:=0 TO 3 DO
	IF SHR(SHL(UINT_TO_BYTE(alarms.eStopButton),7-n),7)=1 THEN 
		alarms.group[22].message:= concat(alarms.group[22].message, parameters.eStopButtonAlarmLabel[n]); // Array Test
		alarms.group[22].message:= concat(alarms.group[22].message,' / ');
	END_IF
	IF n=3 THEN
		alarms.group[22].message := Delete(alarms.group[22].message,3,len(alarms.group[22].message)-2);
	END_IF
END_FOR

alarms.group[23].state:=alarms.eStopBumper<>0;
alarms.group[23].code:=23;
alarms.group[23].level:=3;
alarms.group[23].priority:=3;
alarms.group[23].message:='Bumper Acionado - ';
FOR n:=0 TO 3 DO
	IF SHR(SHL(UINT_TO_BYTE(alarms.eStopBumper),7-n),7)=1 THEN 
		alarms.group[23].message:= concat(alarms.group[23].message, parameters.EStopButtonAlarmLabel[n]);
		alarms.group[23].message:= concat(alarms.group[23].message,' / ');
	END_IF
	IF n=3 THEN
		alarms.group[23].message := Delete(alarms.group[23].message,3,len(alarms.group[23].message)-2);
	END_IF
END_FOR

alarms.group[24].state:=alarms.eStopScanner<>0;
alarms.group[24].code:=24;
alarms.group[24].level:=2;
alarms.group[24].priority:=2;
alarms.group[24].message:='Scanner Atuado - ';
FOR n:=0 TO 4 DO
	IF SHR(SHL(UINT_TO_BYTE(alarms.eStopScanner),7-n),7)=1 THEN 
		alarms.group[24].message:= concat(alarms.group[24].message, parameters.ScannerAlarmLabel[n]);
		alarms.group[24].message:= concat(alarms.group[24].message,' / ');
	END_IF
	IF n=4 THEN
		alarms.group[24].message := Delete(alarms.group[24].message,4,len(alarms.group[24].message)-2);
	END_IF
END_FOR

alarms.group[26].state:=alarms.FaultOverSpeed;
alarms.group[26].code:=26;
alarms.group[26].level:=3;
alarms.group[26].priority:=3;
alarms.group[26].message:='Excesso de Velocidade sem Scanner';

alarms.group[27].state:=alarms.faultFielSetVsSpeed;
alarms.group[27].code:=27;
alarms.group[27].level:=3;
alarms.group[27].priority:=3;
alarms.group[27].message:='Area Scanner incompativel com Velocidade';

alarms.group[28].state:=alarms.faultStandstill;
alarms.group[28].code:=28;
alarms.group[28].level:=2;
alarms.group[28].priority:=2;
alarms.group[28].message:='AGV Parado com Comando de Velocidade';


//Controllers
alarms.group[40].state:=alarms.controllerU1FaultCode<>'';
alarms.group[40].code:=40;
alarms.group[40].level:=2;
alarms.group[40].priority:=2;
alarms.group[40].message:= concat('Controlador U1 - ',alarms.controllerU1FaultCode);

alarms.group[41].state:=alarms.controllerU2FaultCode<>'';
alarms.group[41].code:=41;
alarms.group[41].level:=2;
alarms.group[41].priority:=2;
alarms.group[41].message:= concat('Controlador U2 - ',alarms.controllerU2FaultCode);

alarms.group[42].state:=alarms.controllerU3FaultCode<>'';
alarms.group[42].code:=42;
alarms.group[42].level:=2;
alarms.group[42].priority:=2;
alarms.group[42].message:= concat('Controlador U3 - ',alarms.controllerU3FaultCode);

alarms.group[43].state:=alarms.controllerU4FaultCode<>'';
alarms.group[43].code:=43;
alarms.group[43].level:=2;
alarms.group[43].priority:=2;
alarms.group[43].message:= concat('Controlador U4 - ',alarms.controllerU4FaultCode);

alarms.group[44].state:=alarms.controllerU5FaultCode<>'';
alarms.group[44].code:=44;
alarms.group[44].level:=2;
alarms.group[44].priority:=2;
alarms.group[44].message:= concat('Controlador U5 - ',alarms.controllerU5FaultCode);

alarms.group[45].state:=alarms.controllerU6FaultCode<>'';
alarms.group[45].code:=45;
alarms.group[45].level:=2;
alarms.group[45].priority:=2;
alarms.group[45].message:= concat('Falha Controlador U6 - ',alarms.controllerU6FaultCode);


//Motion Timeouts
alarms.group[46].state:=alarms.faultPositioningTimeoutFront;
alarms.group[46].code:=46;
alarms.group[46].level:=2;
alarms.group[46].priority:=2;
alarms.group[46].message:='Timeout de Direção Dianteira';

alarms.group[47].state:=alarms.faultPositioningTimeoutRear;
alarms.group[47].code:=47;
alarms.group[47].level:=2;
alarms.group[47].priority:=2;
alarms.group[47].message:='Timeout de Direção Traseira';

alarms.group[48].state:=alarms.faultLiftTimout;
alarms.group[48].code:=48;
alarms.group[48].level:=2;
alarms.group[48].priority:=2;
alarms.group[48].message:='Timeout da Elevação';


//Sensors
alarms.group[60].state:=alarms.faultMlsTrackSensor<>'';
alarms.group[60].code:=60;
alarms.group[60].level:=3;
alarms.group[60].priority:=3;
alarms.group[60].message:=concat('Sensor de Navegação - ', alarms.faultMlsTrackSensor);

alarms.group[61].state:=alarms.faultInductiveTrackSensor<>'';
alarms.group[61].code:=61;
alarms.group[61].level:=3;
alarms.group[61].priority:=3;
alarms.group[61].message:=concat('Sensor de Navegação - ', alarms.faultInductiveTrackSensor);

alarms.group[62].state:=alarms.faultLector620 <>'';
alarms.group[62].code:=62;
alarms.group[62].level:=2;
alarms.group[62].priority:=2;
alarms.group[62].message:=concat('Lector620 - ', alarms.faultLector620);

alarms.group[63].state:=alarms.faultDistanceSensor <>'';
alarms.group[63].code:=63;
alarms.group[63].level:=2;
alarms.group[63].priority:=2;
alarms.group[63].message:=concat('Sensor de Distancia - ', alarms.faultDistanceSensor);

alarms.group[64].state:=alarms.faultNoread;
alarms.group[64].code:=64;
alarms.group[64].level:=2;
alarms.group[64].priority:=2;
alarms.group[64].message:='Etiqueta não lida';

//Mission Management
alarms.group[80].state:=alarms.faultNoRoutTabMoveType;
alarms.group[80].code:=80;
alarms.group[80].level:=2;
alarms.group[80].priority:=2;
alarms.group[80].message := 'Tabela de Rotas - Sem Tipo de Movimento';
alarms.group[81].state:=alarms.faultUnexpectedTag;
alarms.group[81].code:=81;
alarms.group[81].level:=2;
alarms.group[81].priority:=2;
alarms.group[81].message := 'Tag Inesperado';
alarms.group[82].state:=alarms.faultRouteTabError;
alarms.group[82].code:=82;
alarms.group[82].level:=2;
alarms.group[82].priority:=2;
alarms.group[82].message := 'Tabela de Rotas - Sem Resolução para Destino';

//Movement
alarms.group[100].state:=alarms.faultNoTrack;
alarms.group[100].code:=100;
alarms.group[100].level:=2;
alarms.group[100].priority:=2;
alarms.group[100].message:='Sem Faixa de Guiamento';
 
alarms.group[101].state:=alarms.faultPositioningTimout;
alarms.group[101].code:=101;
alarms.group[101].level:=2;
alarms.group[101].priority:=2;
alarms.group[101].message:='Timeout de Posicionamento';

alarms.group[102].state:=alarms.faultForkTipCollision;
alarms.group[102].code:=102;
alarms.group[102].level:=2;
alarms.group[102].priority:=2;
alarms.group[102].message:='Colisão Ponta de Garfo';


//Action
alarms.group[120].state:=alarms.faultBoxEmpty;
alarms.group[120].code:=31;
alarms.group[120].level:=2;
alarms.group[120].priority:=2;
alarms.group[120].message:= 'Box Vazio';

alarms.group[121].state:=alarms.faultBoxBusy;
alarms.group[121].code:=121;
alarms.group[121].level:=2;
alarms.group[121].priority:=2;
alarms.group[121].message:= 'Box Ocupado';

alarms.group[122].state:=alarms.faultDidnotUnload;
alarms.group[122].code:=122;
alarms.group[122].level:=2;
alarms.group[122].priority:=2;
alarms.group[122].message:= 'Palete Não Descarregou';

alarms.group[123].state:=alarms.faultLoadLost;
alarms.group[123].code:=123;
alarms.group[123].level:=2;
alarms.group[123].priority:=2;
alarms.group[123].message:= 'Perda do Palete';

alarms.group[124].state:=alarms.faultAlreadyLoad;
alarms.group[124].code:=124;
alarms.group[124].level:=2;
alarms.group[124].priority:=2;
alarms.group[124].message:= 'AGV Carregado com Ação para Carga';

alarms.group[125].state:=alarms.faultPathBlocked;
alarms.group[125].code:=125;
alarms.group[125].level:=2;
alarms.group[125].priority:=2;
alarms.group[125].message:= 'O caminho do AGV está obstruido';


//Compile Alarms
pulse(IN:= NOT pulse.Q, PT:=T#100ms , Q=> , ET=> );

IF NOT pulse.Q THEN
	RETURN;
END_IF

alarms.alarmCount := 0;
alarms.faultCount := 0;
alarms.actualAlarm.priority := 0;
alarms.actualAlarm.code := 0;  
alarms.actualAlarm.message := '';	
alarms.actualAlarm.level := 0;
FOR i:=1 TO alarms.alarmGroupSize DO
	IF alarms.group[alarms.alarmGroupSize+1-i].state=TRUE THEN
		alarms.alarmCount := alarms.alarmCount + 1; // increment alarm counter
		IF alarms.group[alarms.alarmGroupSize+1-i].level >1 THEN
			alarms.faultCount := alarms.faultCount + 1; // increment fault counter
		END_IF
		IF alarms.group[alarms.alarmGroupSize+1-i].priority >= alarms.actualAlarm.priority THEN  
			alarms.actualAlarm := alarms.group[alarms.alarmGroupSize+1-i]; // overwrite actual alarm
		END_IF		   	
	END_IF
END_FOR
variables.AlarmCode := alarms.actualAlarm.code;  
variables.AlarmMessage := alarms.actualAlarm.message;	
variables.AlarmLevel := alarms.actualAlarm.level;]]></ST>
    </Implementation>
    <LineIds Name="FB_AlarmMgt">
      <LineId Id="10" Count="2" />
      <LineId Id="9" Count="0" />
      <LineId Id="481" Count="0" />
      <LineId Id="484" Count="0" />
      <LineId Id="504" Count="0" />
      <LineId Id="499" Count="1" />
      <LineId Id="738" Count="0" />
      <LineId Id="740" Count="1" />
      <LineId Id="739" Count="0" />
      <LineId Id="501" Count="0" />
      <LineId Id="844" Count="0" />
      <LineId Id="846" Count="1" />
      <LineId Id="845" Count="0" />
      <LineId Id="843" Count="0" />
      <LineId Id="502" Count="1" />
      <LineId Id="489" Count="4" />
      <LineId Id="505" Count="0" />
      <LineId Id="494" Count="4" />
      <LineId Id="682" Count="0" />
      <LineId Id="566" Count="2" />
      <LineId Id="858" Count="0" />
      <LineId Id="861" Count="0" />
      <LineId Id="859" Count="0" />
      <LineId Id="862" Count="0" />
      <LineId Id="864" Count="0" />
      <LineId Id="863" Count="0" />
      <LineId Id="860" Count="0" />
      <LineId Id="571" Count="0" />
      <LineId Id="963" Count="11" />
      <LineId Id="583" Count="0" />
      <LineId Id="572" Count="3" />
      <LineId Id="838" Count="0" />
      <LineId Id="576" Count="1" />
      <LineId Id="839" Count="0" />
      <LineId Id="578" Count="0" />
      <LineId Id="156" Count="0" />
      <LineId Id="945" Count="0" />
      <LineId Id="581" Count="0" />
      <LineId Id="580" Count="0" />
      <LineId Id="149" Count="0" />
      <LineId Id="17" Count="1" />
      <LineId Id="215" Count="0" />
      <LineId Id="19" Count="0" />
      <LineId Id="153" Count="1" />
      <LineId Id="160" Count="0" />
      <LineId Id="22" Count="0" />
      <LineId Id="216" Count="0" />
      <LineId Id="23" Count="0" />
      <LineId Id="646" Count="0" />
      <LineId Id="204" Count="0" />
      <LineId Id="526" Count="5" />
      <LineId Id="780" Count="0" />
      <LineId Id="782" Count="0" />
      <LineId Id="896" Count="0" />
      <LineId Id="941" Count="3" />
      <LineId Id="785" Count="0" />
      <LineId Id="783" Count="0" />
      <LineId Id="584" Count="0" />
      <LineId Id="532" Count="4" />
      <LineId Id="804" Count="1" />
      <LineId Id="904" Count="0" />
      <LineId Id="936" Count="3" />
      <LineId Id="585" Count="0" />
      <LineId Id="940" Count="0" />
      <LineId Id="809" Count="0" />
      <LineId Id="537" Count="4" />
      <LineId Id="811" Count="1" />
      <LineId Id="899" Count="0" />
      <LineId Id="932" Count="3" />
      <LineId Id="815" Count="0" />
      <LineId Id="810" Count="0" />
      <LineId Id="586" Count="0" />
      <LineId Id="542" Count="4" />
      <LineId Id="816" Count="1" />
      <LineId Id="903" Count="0" />
      <LineId Id="928" Count="3" />
      <LineId Id="927" Count="0" />
      <LineId Id="587" Count="0" />
      <LineId Id="821" Count="0" />
      <LineId Id="547" Count="4" />
      <LineId Id="822" Count="1" />
      <LineId Id="897" Count="0" />
      <LineId Id="915" Count="0" />
      <LineId Id="826" Count="0" />
      <LineId Id="923" Count="2" />
      <LineId Id="588" Count="0" />
      <LineId Id="827" Count="0" />
      <LineId Id="552" Count="4" />
      <LineId Id="589" Count="0" />
      <LineId Id="557" Count="4" />
      <LineId Id="590" Count="0" />
      <LineId Id="562" Count="3" />
      <LineId Id="506" Count="1" />
      <LineId Id="608" Count="0" />
      <LineId Id="611" Count="5" />
      <LineId Id="641" Count="0" />
      <LineId Id="617" Count="4" />
      <LineId Id="642" Count="0" />
      <LineId Id="622" Count="4" />
      <LineId Id="643" Count="0" />
      <LineId Id="627" Count="4" />
      <LineId Id="644" Count="0" />
      <LineId Id="632" Count="4" />
      <LineId Id="645" Count="0" />
      <LineId Id="637" Count="3" />
      <LineId Id="609" Count="0" />
      <LineId Id="715" Count="13" />
      <LineId Id="730" Count="4" />
      <LineId Id="610" Count="0" />
      <LineId Id="736" Count="0" />
      <LineId Id="735" Count="0" />
      <LineId Id="686" Count="17" />
      <LineId Id="977" Count="4" />
      <LineId Id="976" Count="0" />
      <LineId Id="1008" Count="4" />
      <LineId Id="737" Count="0" />
      <LineId Id="511" Count="0" />
      <LineId Id="57" Count="0" />
      <LineId Id="61" Count="0" />
      <LineId Id="167" Count="0" />
      <LineId Id="63" Count="1" />
      <LineId Id="220" Count="0" />
      <LineId Id="66" Count="0" />
      <LineId Id="168" Count="0" />
      <LineId Id="68" Count="1" />
      <LineId Id="221" Count="0" />
      <LineId Id="71" Count="0" />
      <LineId Id="169" Count="0" />
      <LineId Id="74" Count="0" />
      <LineId Id="59" Count="0" />
      <LineId Id="222" Count="0" />
      <LineId Id="83" Count="0" />
      <LineId Id="75" Count="0" />
      <LineId Id="86" Count="0" />
      <LineId Id="172" Count="0" />
      <LineId Id="88" Count="1" />
      <LineId Id="225" Count="0" />
      <LineId Id="649" Count="4" />
      <LineId Id="92" Count="0" />
      <LineId Id="876" Count="5" />
      <LineId Id="875" Count="0" />
      <LineId Id="431" Count="0" />
      <LineId Id="381" Count="0" />
      <LineId Id="395" Count="4" />
      <LineId Id="744" Count="0" />
      <LineId Id="401" Count="4" />
      <LineId Id="745" Count="0" />
      <LineId Id="407" Count="4" />
      <LineId Id="746" Count="6" />
      <LineId Id="958" Count="4" />
      <LineId Id="743" Count="0" />
      <LineId Id="993" Count="3" />
      <LineId Id="742" Count="0" />
      <LineId Id="390" Count="0" />
      <LineId Id="187" Count="0" />
      <LineId Id="185" Count="0" />
      <LineId Id="189" Count="0" />
      <LineId Id="192" Count="0" />
      <LineId Id="190" Count="0" />
      <LineId Id="193" Count="2" />
      <LineId Id="210" Count="0" />
      <LineId Id="209" Count="0" />
      <LineId Id="237" Count="0" />
      <LineId Id="449" Count="1" />
      <LineId Id="446" Count="0" />
      <LineId Id="196" Count="0" />
      <LineId Id="198" Count="0" />
      <LineId Id="243" Count="3" />
      <LineId Id="232" Count="1" />
      <LineId Id="212" Count="0" />
      <LineId Id="201" Count="0" />
      <LineId Id="199" Count="0" />
      <LineId Id="260" Count="1" />
      <LineId Id="259" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>